from starlette.config import Config

config = Config('.env')

DATABASE_URL: str = config('DATABASE_URL')

RABBITMQ_URL: str = config('RABBITMQ_URL')
PUBLISH_QUEUE: str = config('PUBLISH_QUEUE', default='api')
CONSUME_QUEUE: str = config('CONSUME_QUEUE', default='route_creator')

MIN_COUNT_POINT_FOR_ROUTE_STEP: int = config('MIN_COUNT_POINT_FOR_ROUTE_STEP', default=3)
