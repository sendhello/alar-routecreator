from funcy import first

from config import MIN_COUNT_POINT_FOR_ROUTE_STEP
from db.queries import get_point_by_id_query, get_points_query
from schemas.points import Point
from schemas.routes import Route, RouteOut


def get_min_square_area(start_point_x: int, start_point_y: int, end_point_x: int, end_point_y: int) -> dict:
    """Получение минимальной области координат для двух точек.
    """
    left_point = min(start_point_x, end_point_x)
    right_point = max(start_point_x, end_point_x)
    top_point = min(start_point_y, end_point_y)
    bottom_point = max(start_point_y, end_point_y)

    return {
        'left_point': left_point,
        'right_point': right_point,
        'top_point': top_point,
        'bottom_point': bottom_point,
    }


def create_route(message) -> dict:
    """Высчитывает полный путь от старта до финиша маршрута.
    """
    route = Route.parse_obj(message)

    # Получение из БД координат начальной и конечной точек
    db_start_point = get_point_by_id_query(point_id=route.start_point_id)
    db_end_point = get_point_by_id_query(point_id=route.end_point_id)
    if not db_start_point or not db_end_point:
        raise ValueError('Start or End Point not exists in database')

    start_point = Point.parse_obj(db_start_point.__dict__)
    end_point = Point.parse_obj(db_end_point.__dict__)

    # Получение из БД минимальной области, на которой можно построить маршрут между точками
    min_area = get_min_square_area(
        start_point_x=start_point.pos_x,
        start_point_y=start_point.pos_y,
        end_point_x=end_point.pos_x,
        end_point_y=end_point.pos_y,
    )
    points = [Point.parse_obj(db_point.__dict__) for db_point in get_points_query(**min_area)]

    route_map = [start_point.id]
    # Удаляем стартовую и конечную точки из общего пула точек
    points.remove(start_point)
    # Назначаем стартовую точку текущей
    current_point = start_point

    slice_points_count = max(int(len(points) / 100), MIN_COUNT_POINT_FOR_ROUTE_STEP)

    # Производим поиск маршрута, пока не наткнемся на конечную точку
    while current_point != end_point:
        # Сортировка точек по расстоянию от текущей точки
        sorted_points_from_current_point = sorted(points, key=current_point.get_distance)
        # Берем первые N точек, самых близких к текущей
        points_pul = sorted_points_from_current_point[:slice_points_count]
        # Сортировка отобранных точек по расстояние до конечной точки
        sorted_points_from_end_point = sorted(points_pul, key=end_point.get_distance)
        # Берем самую близкую точку к конечной
        next_point = first(sorted_points_from_end_point)

        # Добавляем найденную точку в маршрут и назначаем ее текущей
        route_map.append(next_point.id)
        current_point = next_point

        # Удаляем найденную точку из общего пула точек
        points.remove(current_point)

    if len(route_map) < 4:
        raise ValueError('Very curt route')

    return RouteOut(id=route.id, points=route_map).dict()
