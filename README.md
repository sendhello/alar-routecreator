# Alar RouteCreator

Принимает через очередь RabbitMQ систему координат и 
начальную и конечную точки маршрута.

Отправляет в очередь готовый маршрут.

### Установка зависимостей
```shell
poetry install
```

### Запуск сервиса
##### локально в виртуальном окружении:
```shell
python main.py
```
##### в docker-compose:
```shell
docker-compose -f docker-compose.yml up
```

## API
### Формат приема сообщений RabbitMQ
```json
{
  "id": 34, 
  "title": "route_name", 
  "start_point_id": 1, 
  "end_point_id": 2
}
```
### Формат передачи сообщений RabbitMQ
```json
{
  "id": 34, 
  "points": [1, 56, 45, 5]
}
```

### Переменные окружения
* `RABBITMQ_URL` - URL-строка соединения с RabbitMQ
* `PUBLISH_QUEUE` - Имя исходящей очереди, по-умолчанию `api`
* `CONSUME_QUEUE` - Имя входящей очереди, по-умолчанию `route_creator`
* `MIN_COUNT_POINT_FOR_ROUTE_STEP` - минимальное количество соседних точек от стартовой 
точки, по которы происходит определения ближайшей точки к конечной при построении
маршрута, по-умолчанию `3`.