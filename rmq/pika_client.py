import json
import logging
import uuid

import pika

from config import CONSUME_QUEUE, PUBLISH_QUEUE, RABBITMQ_URL

logger = logging.getLogger(__name__)


class PikaClient:

    def __init__(self, process_callable):
        self.publish_queue_name = PUBLISH_QUEUE
        url_params = pika.URLParameters(RABBITMQ_URL)
        params = pika.ConnectionParameters(
            host=url_params.host,
            port=url_params.port,
            credentials=url_params.credentials,
            heartbeat=None,
            blocked_connection_timeout=None,
        )
        self.connection = pika.BlockingConnection(params)
        self.channel = self.connection.channel()
        self.publish_queue = self.channel.queue_declare(queue=self.publish_queue_name)
        self.channel.basic_qos(prefetch_count=1)
        self.callback_queue = self.publish_queue.method.queue
        self.response = None
        self.process_callable = process_callable
        logger.info('Pika connection initialized')

    def consume(self):
        """Настройка канала для приема сообщений.
        """
        self.channel.queue_declare(queue=CONSUME_QUEUE)
        self.channel.basic_consume(
            queue=CONSUME_QUEUE,
            auto_ack=False,
            on_message_callback=self.process_incoming_message,
        )

        logger.info('Established pika listener')
        self.channel.start_consuming()

    def process_incoming_message(self, ch, method, properties, body):
        """Обработка входящего сообщения из очереди RabbitMQ.
        """
        try:
            if body:
                logger.info('Received message')
                self.response = self.process_callable(json.loads(body))
                self.channel.basic_ack(method.delivery_tag)

                self.send_message(self.response)
            else:
                error_message = 'Incoming message not have body'
                logger.error(error_message)
                raise RuntimeError(error_message)

        except FileExistsError:
            self.channel.basic_reject(method.delivery_tag)

    def send_message(self, message: dict):
        """Отправка сообщений в очередь RabbitMQ.
        """
        self.channel.basic_publish(
            exchange='',
            routing_key=self.publish_queue_name,
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=str(uuid.uuid4()),
            ),
            body=json.dumps(message),
        )
