from sqlalchemy import Table

from . import Base, metadata


class Point(Base):
    """Таблица 'points' из существующей БД"""
    __table__ = Table('points', metadata, autoload=True)
