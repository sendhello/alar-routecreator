from db import SessionLocal
from db.models import Point


def get_session():
    session = SessionLocal()
    try:
        return session
    finally:
        session.close()


def get_points_query(left_point, right_point, top_point, bottom_point):
    session = get_session()
    return session.query(Point).filter(
        Point.pos_x >= left_point,
        Point.pos_x <= right_point,
        Point.pos_y >= top_point,
        Point.pos_y <= bottom_point,
    ).all()


def get_point_by_id_query(point_id: int):
    session = get_session()
    return session.query(Point).filter(Point.id == point_id).one()
