import logging

from rmq import PikaClient
from utils import create_route

logger = logging.getLogger(__name__)


if __name__ == '__main__':
    pika_client = PikaClient(create_route)
    pika_client.consume()
