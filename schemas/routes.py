from pydantic import BaseModel, Field


class Route(BaseModel):
    id: int = Field(title='ID маршрута')
    title: str = Field(title='Наименование маршрута')
    start_point_id: int = Field(title='ID точки начала маршрута')
    end_point_id: int = Field(title='ID точки конца маршрута')


class RouteOut(BaseModel):
    id: int = Field(title='ID маршрута')
    points: list[int] = Field(title='Маршрут', default_factory=list)
