from pydantic import BaseModel, Field


class Node(BaseModel):
    """Точка маршрута.
    """
    route_id: int = Field(title='ID маршрута')
    pos: int = Field(title='Порядковый номер узла в маршруте')
    point_id: int = Field(title='ID точки карты')
