from math import fabs, sqrt

from pydantic import BaseModel, Field


class Point(BaseModel):
    """Точка карты.
    """
    id: int = Field(title='ID точки')
    pos_x: int = Field(title='Координата X')
    pos_y: int = Field(title='Координата Y')

    class Config:
        orm_mode = True

    def __lt__(self, other):
        if not isinstance(other, Point):
            raise ValueError(f"Compare {type(self)} and {type(other)} not supported")

        self_length = sqrt(self.pos_x + self.pos_y)
        other_length = sqrt(other.pos_x + other.pos_y)
        return self_length < other_length

    def get_distance(self, other: 'Point'):
        cat_x = fabs(self.pos_x - other.pos_x)
        cat_y = fabs(self.pos_y - other.pos_y)
        return sqrt(cat_x * cat_x + cat_y * cat_y)
